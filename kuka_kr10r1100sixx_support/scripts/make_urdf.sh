#!/bin/bash

MODEL=kr10r1100sixx
XSACRO_FILE=../urdf/${MODEL}.xacro
URDF_FILE=../urdf/${MODEL}.urdf
COLLADA_FILE=../urdf/${MODEL}.dae

. ../../../../devel/setup.sh

if [ -f "$URDF_FILE" ]; then
    echo "Remove previously version of urdf-file"
    rm $URDF_FILE
fi

echo "Generate new version of urdf-file"
xacro --inorder $XSACRO_FILE -o $URDF_FILE
