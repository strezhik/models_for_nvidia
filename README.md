### Description

ROS Packages for TRA Robotics models 

### Laboratory Robots

- ABB IRB1200 7/0.7 (abb_irb1200_7_70_support)
- FANUC LRMate 200iD 7L (fanuc_lrmate200i7l_support_
- KUKA KR-10 R1100 sixx (kuka_kr10r1100sixx_support)
- Motoman MH5LF (motoman_mh5lf_support)
- KUKA KR-16 R1610 (kuka_kr16r1610_support)

### Factory robots

- KUKA KR-210 R2700 (kuka_kr210r2700_support)

### Scenes

- kr16_scene: single KUKA KR-16

- two_robots_scene: KR-10 + LRMate200

- four_robots_scene: KR-10 + LRMAte200id + MH5LF + IRB1200 

## Other

- details: sample details in SDF

- laboratory: Gazebo worlds and RViz collision launch file

### Any support package contains folders

- config: ROS basic controllers description
- launch: ROS launch files for models testing
- meshes: STL and DAE meshes
- scripts: helpers for models testing in RViz and Gazebo
- urdf: URDF and XACRO models description

### Using with ROS and Gazebo

#### clone and build

> mkdir catkin_ws

> cd catkin_ws

> mkdir src

> cd src

> git clone git@gitlab.com:strezhik/models_for_nvidia.git

> cd ..

> catkin_make

#### show model in RViz (ex. ABB IRB1200 7/0.7)

> cd src/models_for_nvidia/abb_irb1200_7_70_support/scripts

> . display.sh

#### show model in Gazebo (ex. KUKA KR-16 R1610)

> cd src/models_for_nvidia/kuka_kr16r1610_support/scripts

> . gazebo.sh
